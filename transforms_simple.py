import images
import transforms
def main():
    imagen = input("Introduzca el nombre de la imagen: ")
    funcion = input("Introduzca la funcion que quiera aplicar: mirror, rotate_right, blur, grayscalecafe: ")

    img = images.read_img(imagen)
    if funcion == "mirror":
        imagen_cambiada = transforms.mirror(img)

    if funcion == "rotate_right":
        imagen_cambiada = transforms.rotate_right(img)

    if funcion == "blur":
        imagen_cambiada = transforms.blur(img)

    if funcion == "grayscale":
        imagen_cambiada = transforms.grayscale(img)

    images.write_img(imagen_cambiada, imagen + "_trans.png")

if __name__ == '__main__':
    main()