import images
import transforms
def main():
    imagen = input("Introduzca el nombre de la imagen: ")
    funcion = input("Introduzca la funcion que quieres aplicar: ")

    img = images.read_img(imagen)
    imagen_cambiada = 0

    if funcion == "change_colors":
        valores = input("¿Que color quieres cambiar? (r,g,b) separados por coma: ")
        list_valores : list = valores.split(",")
        list_valores = [int(x) for x in list_valores]
        valores_a_cambiar = tuple(list_valores)
        color = input("¿A que color lo quieres cambiar? (r,g,b) separados por coma: ")
        list_color_nuevo : list = color.split(",")
        list_color_nuevo = [int(x) for x in list_color_nuevo]
        color_nuevo = tuple(list_color_nuevo)
        imagen_cambiada = transforms.change_colors(img, valores_a_cambiar, color_nuevo)
    elif funcion == "rotate_colors":
        incremento = int(input("¿Que incremento quieres aplicar?: "))
        imagen_cambiada = transforms.rotate_colors(img, incremento)
    elif funcion == "shift":
        n_horizontal = int(input("¿Cuanto quieres desplazar horizontalmente?: "))
        n_vertical = int(input("¿Cuanto quieres desplazar verticalmente?: "))
        imagen_cambiada = transforms.shift(img, n_horizontal, n_vertical)
        images.write_img(imagen_cambiada, imagen + "_trans.png")
    elif funcion == "crop":
        coordenada_x = int(input("Coordenada x del recorte: "))
        coordenada_y = int(input("Coordenada y del recorte: "))
        anchura = int(input("Anchura del recorte: "))
        altura = int(input("Altura del recorte: "))
        imagen_cambiada = transforms.crop(img, coordenada_x, coordenada_y, anchura, altura)
    elif funcion == "filter":
        red = float(input("valor por el que multiplicar color rojo : "))
        green = float(input("valor por el que multiplicar color verde : "))
        blue = float(input("valor por el que multiplicar color azul : "))
        imagen_cambiada = transforms.filter(img, red, green, blue)
    elif funcion == "rotate_right":
        imagen_cambiada = transforms.rotate_right(img)
    elif funcion == "mirror":
        imagen_cambiada = transforms.mirror(img)
    elif funcion == "blur":
        imagen_cambiada = transforms.blur(img)
    elif funcion == "grayscale":
        imagen_cambiada = transforms.grayscale(img)
    else:
        print("Funcion no valida")

    images.write_img(imagen_cambiada, imagen + "_trans.png")

if __name__ == '__main__':
    main()