import images
import transforms
def main():
    funcion = input("Introduzca el nombre de la imagen y las transformaciones a realizar separadas por un espacio, en caso de que admita parametros enteros separelos por coma depúes de la funcion: ")
    funciones: list = funcion.split(" ")
    img = images.read_img(funciones[0])
    for i in funciones:
        if i == "change_colors":
            index = funciones.index(i)
            list_valores: list = funciones[index + 1].split(",")
            list_valores = [int(x) for x in list_valores]
            valores_a_cambiar = tuple(list_valores)
            list_color_nuevo: list = funciones[index+2].split(",")
            list_color_nuevo = [int(x) for x in list_color_nuevo]
            color_nuevo = tuple(list_color_nuevo)
            imagen_cambiada = transforms.change_colors(img, valores_a_cambiar, color_nuevo)
            img = imagen_cambiada
        elif i == "rotate_colors":
            index = funciones.index(i)
            incremento = int(funciones[index+1])
            imagen_cambiada = transforms.rotate_colors(img, incremento)
            img = imagen_cambiada
        elif i == "shift":
            index = funciones.index(i)
            desplazamiento:list = funciones[index+1].split(",")
            desplazamiento = [int(x) for x in desplazamiento]
            horizontal = desplazamiento[0]
            vertical = desplazamiento[1]
            imagen_cambiada = transforms.shift(img, horizontal, vertical)
            img = imagen_cambiada
        elif i == "crop":
            index = funciones.index(i)
            valores: list = funciones[index + 1].split(",")
            valores = [int(x) for x in valores]
            coordenada_x = valores[0]
            coordenada_y = valores[1]
            anchura = valores[2]
            altura = valores[3]
            imagen_cambiada = transforms.crop(img, coordenada_x, coordenada_y, anchura, altura)
            img = imagen_cambiada
        elif i == "filter":
            index = funciones.index(i)
            valores: list = funciones[index + 1].split(",")
            valores = [float(x) for x in valores]
            red = valores[0]
            green = valores[1]
            blue = valores[2]
            imagen_cambiada = transforms.filter(img, red, green, blue)
            img = imagen_cambiada
        elif i == "rotate_right":
            imagen_cambiada = transforms.rotate_right(img)
            img = imagen_cambiada
        elif i == "mirror":
            imagen_cambiada = transforms.mirror(img)
            img = imagen_cambiada
        elif i == "blur":
            imagen_cambiada = transforms.blur(img)
            img = imagen_cambiada
        elif i == "grayscale":
            imagen_cambiada = transforms.grayscale(img)
            img = imagen_cambiada

    images.write_img(img, funciones[0] + "_trans.png")

if __name__ == '__main__':
    main()
