# ENTREGA CONVOCATORIA ENERO
Erik Alejandro Sánchez Romero, ea.sanchez.2023@alumnos.urjc.es

enlace al vídeo: https://youtu.be/Obe-pyxQNYg

Requisitos Mínimos:
Método change_colors, Método rotate_right, Método mirror,
Método rotate_colors, Método blur, Método shift, Método crop, Método grayscale, Método filter, Programa Transforms_simple.py,
Programa Transforms_agrs.py, Programa_multi.py 
Requisitos opcionales propios:
Programa ruido.py, programa en el que se define la función ruido que necesita como parametro la imagen y una cantidad que cuanto mayor sea más ruido tendrá la imagen salida de
la función, lo que la función realiza para crear ese efecto de ruido es recorrer la imagen pixel por pixel y en cada uno obtener un
número aleatorio, si ese número es menor que nuestra cantidad (que tiene que ser entre 0 y 1) los valores rgb de ese pixel serán
cambiados a unos aleatorios entre 0 y 255. Además he implementado un main donde se puede ver un ejemplo de como se ejecuta la funcion.