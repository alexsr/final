import images
import random
def ruido(image: list[list[tuple[int, int, int]]], cantidad: float) -> list[list[tuple[int, int, int]]]:
    ruido_image = [[0 for i in range(len(image[0]))] for j in range(len(image))]
    for i in range(len(image)):
        for j in range(len(image[i])):
            ruido_image[i][j] = image[i][j]
    for i in range(len(image)):
        for j in range(len(image[i])):
            if random.random() < cantidad:
                r,g,b = image[i][j]
                ruido_r = random.randint(0,255)
                ruido_g = random.randint(0,255)
                ruido_b = random.randint(0,255)
                ruido_image[i][j] = (ruido_r, ruido_g, ruido_b)
    return ruido_image


def main():
    imagen = input("Escriba el nombre de la imagen: ")
    cantidad = input("Escriba la cantidad de ruido entre 0 y 1: ")
    img = images.read_img(imagen)
    imagen_cambiada = ruido(img, float(cantidad))
    images.write_img(imagen_cambiada, imagen + "_ruido.jpg")


if __name__ == '__main__':
    main()