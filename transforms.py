def change_colors(image: list[list[tuple[int, int, int]]],
                  to_change: tuple[int, int, int],
                  to_change_to: tuple[int, int, int]) -> list[list[tuple[int, int, int]]]:
    changed_colors_image = [[0 for i in range(len(image[0]))] for j in range(len(image))]
    for i in range(len(image)):
        for j in range(len(image[i])):
            changed_colors_image[i][j] = image[i][j]
            if changed_colors_image[i][j] == to_change:
                changed_colors_image[i][j] = to_change_to
    return changed_colors_image

def rotate_right(image: list[list[tuple[int, int, int]]]) -> list[list[tuple[int, int, int]]]:
    rotated_image = [[0 for i in range(len(image))] for j in range(len(image[0]))]
    for i in range(len(image)):
        for j in range(len(image[i])):
            rotated_image[j][len(image) - 1 - i] = image[i][j]
    return rotated_image

def mirror(image: list[list[tuple[int, int, int]]]) -> list[list[tuple[int, int, int]]]:
     mirrored_image = [[0 for i in range(len(image[0]))]for j in range(len(image))]
     for i in range(len(image)):
         for j in range(len(image[i])):
             mirrored_image[len(image)-1-i][j] = image[i][j]
     return mirrored_image
def rotate_colors(image: list[list[tuple[int, int, int]]], increment: int) -> list[list[tuple[int, int, int]]]:
    colors = [(r,g,b) for row in image for (r,g,b) in row]
    for x in range(len(colors)):
        r,g,b = colors[x]
        r = (r + increment) % 256
        g = (g + increment) % 256
        b = (b + increment) % 256
        colors[x] = (r,g,b)
    n = 0
    for i in range(len(image)):
        for j in range(len(image[i])):
            image[i][j] = colors[n]
            n += 1
    return image

def blur(image: list[list[tuple[int, int, int]]]) -> list[list[tuple[int, int, int]]]:
    blurred_image = [[0 for x in range(len(image[0]))] for y in range(len(image))]

    for x in range(len(image)):
        for y in range(len(image[x])):

            red = 0
            green = 0
            blue = 0
            count = 0

            if y - 1 >= 0:
                r, g, b = image[x][y - 1]
                red += r
                green += g
                blue += b
                count += 1

            if y + 1 < len(image[x]):
                r, g, b = image[x][y + 1]
                red += r
                green += g
                blue += b
                count += 1

            if x - 1 >= 0:
                r, g, b = image[x - 1][y]
                red += r
                green += g
                blue += b
                count += 1

            if x + 1 < len(image):
                r, g, b = image[x + 1][y]
                red += r
                green += g
                blue += b
                count += 1

            blurred_image[x][y] = (int(red / count), int(green / count), int(blue / count))

    return blurred_image

def shift(image: list[list[tuple[int, int, int]]], horizontal: int, vertical: int) -> list[list[tuple[int, int, int]]]:
    shifted_image = [[0 for x in range(len(image[0]))] for y in range(len(image))]
    for i in range(len(image)):
        for j in range(len(image[0])):
            new_i = (i + horizontal) % len(image)
            new_j = (j + vertical) % len(image[0])

            shifted_image[i][j] = image[new_i][new_j]
    return shifted_image
def crop(image: list[list[tuple[int, int, int]]], x: int, y: int, width: int, height: int) ->list[list[tuple[int, int, int]]]:
    cropped_image = [[0 for i in range(width)] for j in range(height)]
    for i in range(height):
        for j in range(width):
            if x + j < len(image[0]) and y + i < len(image):
                cropped_image[i][j] = image[y + i][x + j]
    return cropped_image
def grayscale(image: list[list[tuple[int, int, int]]]) -> list[list[tuple[int, int, int]]]:
    grayscale_image = [[0 for i in range(len(image[0]))] for j in range(len(image))]
    for i in range(len(image)):
        for j in range(len(image[i])):
            r, g, b = image[i][j]
            gray = int((r + g + b) / 3)
            grayscale_image[i][j] = (gray, gray, gray)
    return grayscale_image
def filter(image: list[list[tuple[int, int, int]]], r: float, g: float, b: float) ->list[list[tuple[int, int, int]]]:
    filtered_image = [[0 for i in range(len(image[0]))] for j in range(len(image))]
    for i in range(len(image)):
        for j in range(len(image[i])):
            orig_r, orig_g, orig_b = image[i][j]
            new_r = int(orig_r * r)
            new_g = int(orig_g * g)
            new_b = int(orig_b * b)
            if new_r > 255:
                new_r = 255
            if new_g > 255:
                new_g = 255
            if new_b > 255:
                new_b = 255
            filtered_image[i][j] = (new_r, new_g, new_b)
    return filtered_image














